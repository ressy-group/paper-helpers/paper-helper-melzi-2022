with open("from-paper/genbank_accessions.txt") as f_in:
    GB_ACCESSIONS = [line.strip() for line in f_in]

TARGET_GBF_FASTA = expand("from-genbank/{acc}.fasta", acc=GB_ACCESSIONS)

rule make_seqs_sheet:
    output: "output/genbank.csv"
    input: TARGET_GBF_FASTA
    shell: "python scripts/make_genbank_sheet.py {input} > {output}"

rule all_gbf_fasta:
    input: TARGET_GBF_FASTA

rule download_gbf_fa:
    output: "from-genbank/{acc}.fasta"
    shell: "python scripts/download_ncbi.py nucleotide {wildcards.acc} {output}"
