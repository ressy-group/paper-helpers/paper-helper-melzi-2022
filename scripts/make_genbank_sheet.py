#!/usr/bin/env python

"""
Make CSV of antibody seq attributes from downloaded GenBank files.
"""

import re
import sys
from collections import defaultdict
from csv import DictReader, DictWriter
from Bio import SeqIO

def parse_seq_desc(txt):
    pat = r"([A-Z0-9.]+) .*clone ([^ ]+) .*(heavy|lambda|kappa) .*"
    match = re.match(pat, txt)
    keys = ["Accession", "Entry", "Locus"]
    fields = {k: v for k, v in zip(keys, match.groups())}
    fields["Locus"] = "IG" + fields["Locus"][0].upper()
    fields["Chain"] = "heavy" if fields["Locus"] == "IGH" else "light"
    match = re.match(r"[HL]C_d([0-9]+)\.([mA-Z0-9]+)\.m([0-9]+)\.([A-Z0-9]+)", fields["Entry"])
    keys = ["Timepoint", "Immunogen", "MNum", "Label"]
    fields.update({k: v for k, v in zip(keys, match.groups())})
    return fields

FIELDS = ["Entry", "Accession", "Chain", "Locus", "Timepoint", "Immunogen", "MNum", "Label", "Seq"]

def make_seqs_sheet(fastas):
    entries = []
    for fasta in fastas:
        for record in SeqIO.parse(fasta, "fasta"):
            fields = parse_seq_desc(record.description)
            fields["Seq"] = str(record.seq)
            entries.append(fields)
    writer = DictWriter(sys.stdout, FIELDS, lineterminator="\n")
    writer.writeheader()
    writer.writerows(entries)

if __name__ == "__main__":
    make_seqs_sheet(sys.argv[1:])
