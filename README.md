# Antibody GenBank sequences from Melzi 2022

Membrane-bound mRNA immunogens lower the threshold to activate HIV Env V2 apex-directed broadly neutralizing B cell precursors in humanized mice.
Eleonora Melzi et al.
Immunity Volume 55, Issue 11, P2168-2186.e6 November 08, 2022
<https://doi.org/10.1016/j.immuni.2022.09.003>
